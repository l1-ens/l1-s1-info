<!-- ============================== -->
<h3>Supports de cours, et TD sur ordinateur</h3>
<!-- ============================================================ -->

<p class="xlight">L'onglet "Évaluation" fournit un accès aux sujets
de DS des années précédentes.</p>

<!-- ------------------------------ --> 
<h4>0. Environnement de travail</h4>
<!-- ------------------------------------------------------------ --> 

<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/00-accueil.html"
  >Découverte de l'environnement de travail sur ordinateur ;
  inscription sur moodle</a>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/00-thonny.html">Découverte 
  de Thonny, un environnement de programmation dédié à Python</a>
  
  </li>
</ul>

<!-- ------------------------------ --> 
<h4>1. Valeurs, types et variables — Expressions, instructions</h4>
<!-- ------------------------------------------------------------ --> 

Supports de cours 
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/011-val-type-str.html"
  >Valeur et type – chaînes de caractères</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/011-val-type-str.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/012-variable-str.html"
  >Variables</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/012-variable-str.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/013-val-type-int.html"
  >Nombres entiers</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/013-val-type-int.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/014-val-type-float.html"
  >Nombres à virgule flottante</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/014-val-type-float.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/015-expr-instr.html"
  >Expressions et instructions</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/015-expr-instr.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

Exercices
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/01x1-val-var-expr.html"
  >Niveau débutant</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/01x1-val-var-expr.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/01x2-val-var-expr.html"
  >Niveau intermédiaire</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/01x2-val-var-expr.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

<span class="light">Ensemble des supports <a class="fichier" href=
"https://www.fil.univ-lille.fr/~L1S1Info/last/cours/010-valvar.pdf"
><span class="xlight">version PDF noir et blanc</span></a> pour impression</span>

<!-- ------------------------------ --> 
<h4>2. Fonctions</h4>
<!-- ------------------------------------------------------------ -->

Supports de cours 
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/021-fonction.html"
  >Utilisation de fonctions</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/021-fonction.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/022-fonction.html"
  >Bibliothèque et importation de fonctions</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/022-fonction.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/023-fonction.html"
  >Définition de fonctions</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/023-fonction.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/024-fonction.html"
  >Test de fonctions</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/024-fonction.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

Exercices
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/02x1-fonction.html"
  >Niveau débutant</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/02x1-fonction.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/02x2-fonction.html"
  >Niveau intermédiaire</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/02x2-fonction.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/02x3-fonction.html"
  >Niveau confirmé</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/02x3-fonction.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

<span class="light">Ensemble des supports <a class="fichier" href=
"https://www.fil.univ-lille.fr/~L1S1Info/last/cours/020-fonction.pdf"
><span class="xlight">version PDF noir et blanc</span></a> pour impression</span>

<!-- ------------------------------ -->
<h4>3. Type booléen - Expressions booléennes - Prédicats</h4>
<!-- ------------------------------------------------------------ -->

Supports de cours
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/031-bool.html"
  >Expressions booléennes</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/031-bool.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/032-bool.html"
  >Opérateurs logiques</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/032-bool.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/033-bool.html"
  >Expressions booléennes et doctests</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/033-bool.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

Exercices
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/03x1-bool.html"
  >Niveau débutant</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/03x1-bool.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/03x2-bool.html"
  >Niveau intermédiaire</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/03x2-bool.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/03x3-bool.html"
  >Niveau confirmé</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/03x3-bool.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

<span class="light">Ensemble des supports <a class="fichier" href=
"https://www.fil.univ-lille.fr/~L1S1Info/last/cours/030-bool.pdf"
><span class="xlight">version PDF noir et blanc</span></a> pour impression</span>

<!-- ------------------------------ -->
<h4>4. Instruction conditionnelle</h4>
<!-- ------------------------------------------------------------ -->

Supports de cours
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/041-if.html"
  >Instruction conditionnelle</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/041-if.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/042-if.html"
  >Instruction conditionnelle et alternative</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/042-if.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/043-if.html"
  >Instruction conditionnelle et alternatives multiples</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/043-if.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

Exercices
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/04x1-if.html"
  >Niveau débutant</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/04x1-if.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/04x2-if.html"
  >Niveau intermédiaire</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/04x2-if.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/04x3-if.html"
  >Niveau confirmé</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/04x3-if.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

<span class="light">Ensemble des supports <a class="fichier" href=
"https://www.fil.univ-lille.fr/~L1S1Info/last/cours/040-if.pdf"
><span class="xlight">version PDF noir et blanc</span></a> pour impression</span>

<!-- ------------------------------ -->
<h4>5. Interaction avec l'utilisateur</h4>
<!-- ------------------------------------------------------------ -->

Supports de cours
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/051-io.html"
  >Interaction avec l'utilisateur — Programme </a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/051-io.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/052-io.html"
  >Écrire sur le terminal</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/052-io.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/053-io.html"
  >Lire depuis le terminal</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/053-io.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

Exercices
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/05x1-io.html"
  >Niveau débutant</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/05x1-io.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/05x2-io.html"
  >Niveau intermédiaire</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/05x2-io.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/05x3-io.html"
  >Niveau confirmé</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/05x3-io.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

<span class="light">Ensemble des supports <a class="fichier" href=
"https://www.fil.univ-lille.fr/~L1S1Info/last/cours/050-io.pdf"
><span class="xlight">version PDF noir et blanc</span></a> pour impression</span>

<!-- ------------------------------ -->
<h4>6. Boucles - for</h4>
<!-- ------------------------------------------------------------ -->

Supports de cours
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/061-for.html"
  >Itérable et boucle for</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/061-for.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/062-for.html"
  >Intervalle range</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/062-for.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/063-for.html"
  >Utilisations classiques de boucles for</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/063-for.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

    <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/064-for.html"
  >Boucles for imbriquées</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/064-for.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

Exercices
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/06x1-for.html"
  >Niveau débutant</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/06x1-for.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/06x2-for.html"
  >Niveau intermédiaire</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/06x2-for.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/06x3-for.html"
  >Niveau confirmé</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/06x3-for.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/06x4-for.html"
  >Hors-série : dessiner avec des boucles</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/06x4-for.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

<span class="light">Ensemble des supports <a class="fichier" href=
"https://www.fil.univ-lille.fr/~L1S1Info/last/cours/060-for.pdf"
><span class="xlight">version PDF noir et blanc</span></a> pour impression</span>

<!-- ------------------------------ -->
<h4>7. Chaînes de caractères</h4>
<!-- ------------------------------------------------------------ -->

Supports de cours
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/071-str.html"
  >Rappels sur les chaînes de caractères</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/071-str.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/072-str.html"
  >Indices pour les chaînes de caractères</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/072-str.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/073-str.html"
  >Codage des caractères et comparaison des chaînes de caractères</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/073-str.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/074-str.html"
  >Non mutabilité des chaînes de caractères</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/074-str.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

Exercices
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/07x1-str.html"
  >Niveau débutant</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/07x1-str.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/07x2-str.html"
  >Niveau intermédiaire</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/07x2-str.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/07x3-str.html"
  >Niveau confirmé</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/07x3-str.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

<span class="light">Ensemble des supports <a class="fichier" href=
"https://www.fil.univ-lille.fr/~L1S1Info/last/cours/070-str.pdf"
><span class="xlight">version PDF noir et blanc</span></a> pour impression</span>

<!-- ------------------------------ -->
<h4>8. Itération conditionnelle - while</h4>
<!-- ------------------------------------------------------------ -->

Supports de cours
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/081-while.html"
  >Itération conditionnelle</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/081-while.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/082-while.html"
  >Utilisations typiques de boucles while</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/082-while.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/083-while.html"
  >Autres utilisations typiques de boucles while</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/083-while.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

Exercices
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/08x1-while.html"
  >Niveau débutant</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/08x1-while.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/08x2-while.html"
  >Niveau intermédiaire</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/08x2-while.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/08x3-while.html"
  >Niveau confirmé</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/08x3-while.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

<span class="light">Ensemble des supports <a class="fichier" href=
"https://www.fil.univ-lille.fr/~L1S1Info/last/cours/080-while.pdf"
><span class="xlight">version PDF noir et blanc</span></a> pour impression</span>

<!-- ------------------------------ -->
<h4>9. Listes Python - 1re partie</h4>
<!-- ------------------------------------------------------------ -->

Supports de cours
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/091-list.html"
  >Découvrir les listes</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/091-list.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/092-list.html"
  >Modifier une liste</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/092-list.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

Exercices
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/09x1-list.html"
  >Niveau débutant</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/09x1-list.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/09x2-list.html"
  >Niveau intermédiaire</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/09x2-list.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/09x3-list.html"
  >Niveau confirmé</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/09x3-list.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

<span class="light">Ensemble des supports <a class="fichier" href=
"https://www.fil.univ-lille.fr/~L1S1Info/last/cours/090-list.pdf"
><span class="xlight">version PDF noir et blanc</span></a> pour impression</span>

<!-- ------------------------------ -->
<h4>10. Listes Python - 2de partie</h4>
<!-- ------------------------------------------------------------ -->

Supports de cours
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/101-list.html"
  >Aliasing de listes, valeurs mutables</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/101-list.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/102-list.html"
  >Copier une liste</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/102-list.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>

  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/103-list.html"
  >Copier ou muter une liste ?</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/103-list.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>  
</ul>

Autres supports à venir...
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/104-list.html"
  >Listes de listes</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/cours/104-list.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

Exercices
<ul>
  <li><a href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/10x-list.html"
  >Tous niveaux</a>
  <span class="xlight"> ou <a class="fichier" href=
  "https://www.fil.univ-lille.fr/~L1S1Info/last/exos/10x-list.pdf"
  ><span class="xlight" >version PDF</span></a> pour impression</span>
  </li>
</ul>

<span class="light">Ensemble des supports <a class="fichier" href=
"https://www.fil.univ-lille.fr/~L1S1Info/last/cours/100-list.pdf"
><span class="xlight">version PDF noir et blanc</span></a> pour impression</span>

<!-- ============================== -->
<!-- <h3>À venir...</h3> -->
<!-- ============================================================ -->

<!-- ------------------------------ -->
<!-- <h4>11. Recherche d'un élément dans une liste</h4> -->
<!-- ------------------------------------------------------------ -->

<!-- ------------------------------ -->
<!-- <h4>Type dictionnaire</h4> -->
<!-- ------------------------------------------------------------ -->

<!-- ------------------------------ -->
<!-- <h4>Recherche d'un élément dans une liste de dictionnaires</h4> -->
<!-- ------------------------------------------------------------ -->

<!-- ------------------------------ -->
<!-- <h4>Fichiers</h4> --> 
<!-- ------------------------------------------------------------ -->

<!-- ------------------------------ -->
<!-- <h4>Consolidation et révisions</h4> -->
<!-- ------------------------------------------------------------ -->


<?php
  include("https://gitlab-fil.univ-lille.fr/l1-ens/l1-s1-info/-/raw/master/portail/signature.php");
?>   
