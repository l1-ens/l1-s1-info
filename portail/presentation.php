<!-- Le nom de l'UE -->
<div class="UE">Informatique</div>
<div class="bcc">BCC 1 : Appréhender les approches disciplinaires pour cerner leurs spécificités et leurs complémentarités</div>

<h3> Pré-requis </h3>
Il n'y a pas de pré-requis pour cette UE à part savoir se servir d'un ordinateur.

<!-- Volume horaire et organisation de l'UE C, CTD, TD, TP -->
<h3> Organisation  </h3>

<p>
Cette unité se déroule au S1 du portail SESI (UE optionnelle), 
des premières années de la licence MIASHS et de PEIP.
</p>

<p>
Volume horaire : 1h30 de cours magistral et deux séances de 1h30 de TD 
sur ordinateur par semaine, pendant 12 semaines.</p>

<p>
Cette UE constitue un pré-requis pour les UE d'informatique 
proposées au second semestre de SESI, MIASHS et PEIP.
<!--
l'<a href="http://portail.fil.univ-lille1.fr/????????">UE XXX</a>.
-->

</p>


<h3> Crédits </h3>

<strong>6 ECTS</strong>


<!-- le ou les responsable(s) -->
<h3> Responsable </h3>

<p>
Maude Pupin, Bât. M3 extension, bureau 208, e-mail : <tt>maude.pupin [AT] univ-lille.fr </tt>
</p>


<?php
  include("https://gitlab-fil.univ-lille.fr/l1-ens/l1-s1-info/-/raw/master/portail/signature.php");
?>   
