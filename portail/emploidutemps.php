<div class="edt">     

<table class="table">
<tr class="entete">
<th> Gpe </th>
<th> Nature </th>
<th> Horaire </th>
<th> Salle </th>
<th> Enseignant </th>
</tr>

<tr class="CTD" >
<td rowspan=3> <!-- GROUPE  --> Licam  </td>
<td> <!-- NATURE  --> CTD </td>
<td> <!-- HORAIRE --> lundi 8h-10h</td>
<td> <!-- SALLE   --> SN1 227/229 </td>
<td rowspan=3> <!-- ENSEIGNANT --> Léopold Weinberg</td>
</tr>
<!-- TP -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> lundi 16h45-18h45</td>
<td> <!-- SALLE   --> SUP-117 </td>
</tr>
<!-- TP -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> jeudi 15h-16h30</td>
<td> <!-- SALLE   --> SUP-117 </td>
</tr>


<!-- Cours -->
<tr class="cours" >
<td> <!-- GROUPE  --> PEIP </td>
<td> <!-- NATURE  --> Cours </td>
<td> <!-- HORAIRE --> mercredi 8h30-10h </td>
<td> <!-- SALLE   --> M1-Archimède </td>
<td> <!-- ENSEIGNANT --> Pierre Allegraud </td>
</tr>

<!-- TP -->
<tr class="TD" >
<td> <!-- GROUPE  --> PeiP11 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mercredi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-116 </td>
<td> <!-- ENSEIGNANT --> Anne Etien </td>
</tr>


<!-- TP -->
<tr class="TD" >
<td> <!-- GROUPE  --> PeiP12 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mercredi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-117 </td>
<td> <!-- ENSEIGNANT --> Iona Thomas </td>
</tr>


<!-- TP -->
<tr class="TD" >
<td> <!-- GROUPE  --> PeiP13 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> jeudi 10h15-11h45 </td>
<td> <!-- SALLE   --> SUP-118 </td>
<td> <!-- ENSEIGNANT --> Pierre Allegraud </td>
</tr>


<!-- TP -->
<tr class="TD" >
<td> <!-- GROUPE  --> PeiP14 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> jeudi 10h15-11h45 </td>
<td> <!-- SALLE   --> SUP-120 </td>
<td> <!-- ENSEIGNANT --> Damien Pollet </td>
</tr>


<!-- TP -->
<tr class="TD" >
<td> <!-- GROUPE  --> PeiP15 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> vendredi 13h ou 14h45 </td>
<td> <!-- SALLE   --> SUP-118 </td>
<td> <!-- ENSEIGNANT --> Michel Fryziel </td>
</tr>


<!-- Cours -->
<tr class="cours" >
<td> <!-- GROUPE  --> MIASHS </td>
<td> <!-- NATURE  --> Cours </td>
<td> <!-- HORAIRE --> mardi 14h45-16h15 </td>
<td> <!-- SALLE   --> M1 amphi Galois </td>
<td> <!-- ENSEIGNANT --> Hela Kadri </td>
</tr>

<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> MIASHS1 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> lundi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-120 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Jean-François Roos </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mercredi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-120 </td>
</tr>

<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> MIASHS2 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> lundi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-118 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Ikram Senoussaoui </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mercredi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-118 </td>
</tr>

<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> MIASHS3 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mardi 10h15-11h45 </td>
<td> <!-- SALLE   --> SUP-120 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Claire Divoy </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> jeudi 8h30-10h </td>
<td> <!-- SALLE   --> SUP-120 </td>
</tr>

<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> MIASHS4 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mardi 10h15-11h45 </td>
<td> <!-- SALLE   --> SUP-124 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Hela Kadri </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> vendredi 8h30-10h </td>
<td> <!-- SALLE   --> SUP-124 </td>
</tr>

<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> MIASHS5 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> lundi 8h30-10h </td>
<td> <!-- SALLE   --> SUP-124 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Arnaud Liefooghe </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mardi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-124 </td>
</tr>

<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> MIASHS6 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> lundi 8h30-10h </td>
<td> <!-- SALLE   --> SUP-120 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Fabrice Aubert </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mardi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-120 </td>
</tr>

<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> MIASHS7 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> lundi 8h30-10h </td>
<td> <!-- SALLE   --> SUP-118 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Laetitia Jourdan </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mardi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-118 </td>
</tr>

<tr class="cours" >
<td> <!-- GROUPE  --> PCI </td>
<td> <!-- NATURE  --> Cours </td>
<td> <!-- HORAIRE --> mardi 13h-14h30 </td>
<td> <!-- SALLE   --> M1-Galois </td>
<td> <!-- ENSEIGNANT --> Claire Divoy </td>
</tr>


<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI12 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mardi 14h45-16h15 </td>
<td> <!-- SALLE   --> SUP-118 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Claire Divoy </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> jeudi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-118 </td>
</tr>


<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI13 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> jeudi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-124 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Maude Pupin </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> jeudi 14h45-16h15</td>
<td> <!-- SALLE   --> SUP-124 </td>
</tr>


<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI14 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> jeudi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-117 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Émilie Allart </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> vendredi 8h30-10h </td>
<td> <!-- SALLE   --> SUP-120 </td>
</tr>


<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI15 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> lundi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-117 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Maxime Savary-Leblanc </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mercredi 10h15-11h45 </td>
<td> <!-- SALLE   --> SUP-124 </td>
</tr>

<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI16 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mercredi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-124 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Arnaud Deleruyelle </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> jeudi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-120 </td>
</tr>


<tr class="cours" >
<td> <!-- GROUPE  --> PISi-section4 </td>
<td> <!-- NATURE  --> Cours </td>
<td> <!-- HORAIRE --> lundi 8h30-10h </td>
<td> <!-- SALLE   --> SUP-14 </td>
<td> <!-- ENSEIGNANT --> Cristian Versari </td>
</tr>

<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI42 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> lundi 10h15-11h45 </td>
<td> <!-- SALLE   --> SUP-118 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Francesco De Comité </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> vendredi 8h30-10h </td>
<td> <!-- SALLE   --> SUP-118 </td>
</tr>
	

<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI43 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> lundi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-124 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Cristian Versari </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> lundi 14h45-16h15 </td>
<td> <!-- SALLE   --> SUP-124 </td>
</tr>


<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI44 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> lundi 14h45-16h15 </td>
<td> <!-- SALLE   --> SUP-118 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Patricia Plénacoste </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> vendredi - variable </td>
<td> <!-- SALLE   --> SUP-117 </td>
</tr>


<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI45 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mercredi 8h30-10h </td>
<td> <!-- SALLE   --> SUP-118 </td>
<td> <!-- ENSEIGNANT --> Francesco Maccarini </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> vendredi 14h45-16h15 </td>
<td> <!-- SALLE   --> SUP-118 </td>
<td> <!-- ENSEIGNANT --> Émilie Allart </td>
</tr>


<tr class="cours" >
<td> <!-- GROUPE  --> PISi-section5 </td>
<td> <!-- NATURE  --> Cours </td>
<td> <!-- HORAIRE --> lundi 8h30-10h </td>
<td> <!-- SALLE   --> SUP-13 </td>
<td> <!-- ENSEIGNANT --> Maude Pupin </td>
</tr>


<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI51 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mercredi 16h30-18h </td>
<td> <!-- SALLE   --> SUP-118 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Benoît Papegay </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> vendredi 10h15-11h45 </td>
<td> <!-- SALLE   --> SUP-118 </td>
</tr>


<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI52 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mercredi 16h30-18h </td>
<td> <!-- SALLE   --> SUP-120 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Philippe Marquet </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> vendredi 10h15-11h45 </td>
<td> <!-- SALLE   --> SUP-120 </td>
</tr>


<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI53 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mercredi 16h30-18h </td>
<td> <!-- SALLE   --> SUP-124 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Pierre Allegraud </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> vendredi 10h15-11h45 </td>
<td> <!-- SALLE   --> SUP-124 </td>
</tr>


<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI54 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mercredi 8h30-10h </td>
<td> <!-- SALLE   --> SUP-120 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Jean-Luc Dekeyser </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> vendredi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-117 </td>
</tr>


<tr class="cours" >
<td> <!-- GROUPE  --> CISi </td>
<td> <!-- NATURE  --> Cours </td>
<td> <!-- HORAIRE --> mardi 8h30-10h </td>
<td> <!-- SALLE   --> M1-Painlevé </td>
<td> <!-- ENSEIGNANT --> Mirabelle Nebut </td>
</tr>


<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI41B </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mercredi 14h45-16h15 </td>
<td> <!-- SALLE   --> SUP-120 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Adrien Luxey </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> vendredi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-120 </td>
</tr>

<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI11B+61B </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mardi 14h45-16h15 </td>
<td> <!-- SALLE   --> SUP-124 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Alice Loizeau </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mercredi 14h45-16h15 </td>
<td> <!-- SALLE   --> SUP-117 </td>
</tr>

<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI61 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mercredi 14h45-16h15 </td>
<td> <!-- SALLE   --> SUP-124 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Laurent Noé </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> vendredi 13h-14h30 </td>
<td> <!-- SALLE   --> SUP-124 </td>
</tr>


<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI62 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mardi 10h15-11h45 </td>
<td> <!-- SALLE   --> SUP-117 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Mirabelle Nebut </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> jeudi 14h45-16h15 </td>
<td> <!-- SALLE   --> SUP-120 </td>
</tr>


<!-- TDO -->
<tr class="TD" >
<td rowspan=2> <!-- GROUPE  --> SESI63 </td>
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> mardi 10h15-11h45 </td>
<td> <!-- SALLE   --> SUP-118 </td>
<td rowspan=2> <!-- ENSEIGNANT --> Patricia Plénacoste </td>
</tr>
<!-- TDO -->
<tr class="TD" >
<td> <!-- NATURE  --> TDO </td>
<td> <!-- HORAIRE --> jeudi 14h45-16h15 </td>
<td> <!-- SALLE   --> SUP-118 </td>
</tr>


</table>

</div><!-- edt -->

<?php
  include("https://gitlab-fil.univ-lille.fr/l1-ens/l1-s1-info/-/raw/master/portail/signature.php");
?>   
