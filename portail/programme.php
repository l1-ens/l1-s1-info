<!-- objectifs de l'UE -->
<h3> Objectifs </h3>

Cette première UE d'informatique à l'Université a pour objectif de faire 
découvrir la programmation et l'algorithmique aux débutants, tout en 
faisant progresser les autres (consolidation des acquis). Le langage 
de programmation utilisé est Python.

<!-- le contenu -->
<h3> Contenu </h3>

<ul>
   <li>utilisation de différents types de données (entiers, flottants, booléens, chaînes de caractères, listes, dictionnaires)</li>
   <li>choix de la structure algorithmique adaptée au problème posé (séquence, choix, itération)</li>
   <li>découpage en sous-programmes</li>
   <li>découverte d'algorithmes pour la recherche d'un ou plusieurs
   éléments dans un tableau</li>
   <li>notion de mutabilité</li>
</ul>

<!-- bibliographie accompagnant l'UE -->
<h3> Liens utiles </h3>

<ul>
   <li>Le <a href="https://moodle.univ-lille.fr/course/view.php?id=34531" target="_blank">cours moodle</a> de l'UE</li>
   <li>Le site officiel de <a href="https://www.python.org/" target="_blank">Python</a></li>
   <li><a href="https://thonny.org/" target="_blank">Thonny</a>, l'environement de programmation utilisé en salle informatique</li>
</ul>


<?php
  include("https://gitlab-fil.univ-lille.fr/l1-ens/l1-s1-info/-/raw/master/portail/signature.php");
?>   
