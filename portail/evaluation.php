<!-- exemple de description des modalités d'évaluation d'une UE -->
<!-- la classe NOTE attribue une couleur -->
<!-- la classe FORMULE pour les tags  -->

<h3> Modalités d'évaluation des connaissances</h3>
<p>
Trois notes seront attribuées à chaque étudiant durant le semestre :
</p>

<ul>
<li><span class="NOTE">TP</span> : une note sur 20 de Travaux Pratiques
 attribuée par l'enseignant de Travaux Pratiques.</li> 
<li>Devoir surveillé intermédiaire (<span class="NOTE">DSi</span>) : 
une note sur 20 d'un devoir surveillé, commun à tous les groupes,
organisé à mi-semestre.</li>
<li>Devoir surveillé final (<span class="NOTE">DSf</span>) : 
une note sur 20 d'un devoir surveillé, commun à tous les groupes,
organisé en fin de semestre.</li> 
</ul>

<p>
La note finale de première session (<span class="NOTE">N1</span>) sur 20 est calculée comme suit :
</p>

<p class="FORMULE">
 N1 = 20% TP + 80% * SUP(50% DSi + 50% DSf ; DSf)
</p>

<p>La session de rattrapage, un devoir surveillé (<span class="NOTE">DSr</span>), remplace les notes de <i>DSi</i> et <i>DSf</i>, seule
la note de <i>TP</i> est conservée.
La note finale de la session de rattrapage (<span class="NOTE">Nr</span>) sur 20 est calculée comme suit :
</p>

<p class="FORMULE">
 Nr = 20% TP + 80% DSr
</p>
 

<p>
L'unité acquise apporte 6 ECTS.
</p>

<h3> Sujets de DS pour le nouveau contenu</h3>

<ul>
  
  <li>2021-2022
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/dsi_21-22.pdf" >DS intermédiaire</a>
    (la version <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/dsi_en_21-22.pdf">anglaise</a>),
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/dsf_21-22.pdf">DS final</a>
    (la version <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/dsf_en_21-22.pdf">anglaise</a>),
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/dss_21-22.pdf">DS de substitution</a> (absence due à la COVID)
    et <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/dsr_21-22.pdf">DS de rattrapage</a> (seconde session)
    
  </li>
  <li>2020-2021
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/dsf_20-21.pdf">DS final</a>
    (la version <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/dsf_en_20-21.pdf">anglaise</a>)
    et <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/dsr_20-21.pdf">DS de rattrapage</a> (seconde session)
  </li>  
</ul>


<h3> Sujets de DS de l'ancienne UE</h3>

<p>Nous vous donnons accès aux sujets de DS des années précédentes (oct 2014 à juin 2020). 
Ces sujets portent sur l'ancienne version de l'UE, avec un contenu
un peu différent. Il se peut donc que vous ne sachiez pas répondre à
certaines questions lorsque celles-ci portent sur une notion qui n'est
plus au programme de la nouvelle UE qui a commencé en septembre 2020.
De plus, la nouvelle UE introduit de nouvelles notions, telles que les listes, et donc celles-ci 
n'apparaissent jamais dans les énoncés ci-dessous. 
Ces notions seront tout-de-même au programme des DS à partir de septembre 2020.<br>

Les DS1 étaient réalisés en milieu de semestre, les DS2 en fin de semestre
et les DS3 lors des sessions de rattrapage. Le seul document autorisé est le memento 
<a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/mementopython.pdf">suivant</a>
imprimé et non annoté.<br>

L'équipe pédagogique a décidé de ne pas diffuser les corrections des ces
sujets. 
</p> 

<ul>
  <li> 2014-2015
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds1_14-15.pdf">DS1</a>,
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds2_14-15.pdf">DS2</a> et
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds3_14-15.pdf">DS3</a> (seconde session)
  </li>
  <li> 2015-2016
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds1_15-16.pdf">DS1</a>,
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds2_15-16.pdf">DS2</a> et
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds3_15-16.pdf">DS3</a> (seconde session)
  </li>
  <li> 2016-2017
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds1_16-17.pdf">DS1</a>,
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds2_16-17.pdf">DS2</a> et 
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds3_16-17.pdf">DS3</a> (seconde session)
  </li>
    <li> 2017-2018
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds1_17-18.pdf">DS1</a>
    (la version <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds1_en_17-18.pdf">anglaise</a>),
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds2_17-18.pdf">DS2</a>
    (la version <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds2_en_17-18.pdf">anglaise</a>)
    et <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds3_17-18.pdf">DS3</a> (seconde session)
  </li>
  <li> 2018-2019
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds1_18-19.pdf">DS1</a>
    (la version <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds1_en_18-19.pdf">anglaise</a>),
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds2_18-19.pdf">DS2</a>
    (la version <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds2_en_18-19.pdf">anglaise</a>)
    et <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds3_18-19.pdf">DS3</a> (seconde session)
  </li>
  <li> 2019-2020
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds1_19-20.pdf">DS1</a>
    (la version <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds1_en_19-20.pdf">anglaise</a>),
    <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds2_19-20.pdf">DS2</a>
    (la version <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds2_en_19-20.pdf">anglaise</a>)
    et <a href="http://www.fil.univ-lille.fr/~L1S1Info/Doc/DS/ds3_19-20.pdf">DS3</a> (seconde session)
  </li>
  
</ul>


<?php
  include("https://gitlab-fil.univ-lille.fr/l1-ens/l1-s1-info/-/raw/master/portail/signature.php");
?>   


