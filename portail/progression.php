<!-- ============================== -->
<h3>Progression</h3>
<!-- ============================================================ -->

<p>Les enseignements sont basés sur la progression présentées
ci-dessous.</p>

<p>Les supports de cours et exercises associés aux notions introduites
sont accessibles via l'onglet "Documents".
</p>

<p>Les annales des années précédentes sont accessibles via l'onglet
"Évaluation".</p>

<ul>
  <li>Valeurs, types et variables — Expressions, instructions</li>
  <li>Fonctions</li>
  <li>Type booléen - Expressions booléennes - Prédicats</li>
  <li>Instruction conditionnelle</li>
  <li><em> Facultatif</em> — Interaction avec l'utilisateur</li>
  <li>Boucles - for</li>
  <li>Chaînes de caractères</li>
  <li>Itération conditionnelle - while</li>
  <li>Consolidation et synthèse <em>travail à partir des annales</em></li>
  <li>Listes Python - 1re partie</li>
  <li>Listes Python - 2de partie</li>
  <li>Recherche d'un élément dans une liste</li>
  <li>Révisions</li>
</ul>

<?php
  include("https://gitlab-fil.univ-lille.fr/l1-ens/l1-s1-info/-/raw/master/portail/signature.php");
?>   
